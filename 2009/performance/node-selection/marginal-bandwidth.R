## Load in data files
tt <- read.table("opt_tor.pickle.dat", header=TRUE)

## Node bandwidth
bw <- tt$bw

## Calculate network capcity
capacity <- sum(bw)

## Calculate selection probabilties
torProb <- bw/capacity
optProb <- tt$prob

## Calculate bandwidth given to each user,
##  assuming 1 million users
torShare <- bw/(torProb*1e6)
optShare <- bw/(optProb*1e6)

## Output graph
col <- rainbow(8)
ylim <- range(c(torShare, optShare), finite=TRUE)
pdf("marginal-bandwidth.pdf")
plot(bw, torShare, ylim=ylim, ylab="Bandwidth per user (cells/s)",
     log="y", type="l", xlab="Node bandwidth (cells/s)",
     main="Comparison of bandwidth at each node,\nfor Tor and Optimized weighting algorithms",
     frame.plot=FALSE, lwd=2)
lines(bw, optShare, type="b", col=col[8])

par(xpd=NA)
text(x=rep(max(bw), 2), y=c(torShare[length(bw)], optShare[length(bw)]), pos=3, c("Tor", "Optimized"))

## Close output device
dev.off()
