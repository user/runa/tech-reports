\documentclass{tortechrep}

\usepackage{doc}
\usepackage{upgreek}
\usepackage{xspace}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{graphicx}

\usepackage{prettyref}
%% Non-breaking space should really be used
\newrefformat{sec}{Section~\ref{#1}}
%% Page numbers seem a bit redundant in a short paper
\newrefformat{tab}{Table~\ref{#1}}
\newrefformat{fig}{Figure~\ref{#1}}
\newrefformat{cha}{Chapter~\ref{#1}}
\newrefformat{app}{Appendix~\ref{#1}}

\makeatletter
\newcommand{\ie}{i.e.\@\xspace}
\newcommand{\eg}{e.g.\@\xspace}
\newcommand{\etc}{etc.\@\xspace}
\newcommand{\cf}{cf.\@\xspace}
\newcommand{\vs}{vs.\@\xspace}
\newcommand{\wrt}{w.r.t.\@\xspace}
\newcommand{\etal}{\textit{et al.\@\xspace}}
\newcommand{\detal}{\textit{et al.}}
\newcommand{\ia}{inter alia\xspace}
\makeatother

\begin{document}

\title{Datagram Testing Plan}
\author{Steven J. Murdoch}
\date{March 16, 2012}
\contact{steven.murdoch@cl.cam.ac.uk}
\reportid{2012-03-002}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle
\thispagestyle{empty}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------------------------------------------------------------
% Abstract
%

%------------------------------------------------------------------------------
\section{Introduction}
\label{sec:introduction}

This document discusses current considerations and plans for evaluating approaches for moving to a datagram transport in Tor.
For background, refer to Murdoch~\cite{datagram-comparison}.

\subsection{Simulation vs Emulation}

An important design choice for the experiments is whether we run experiments on simulated Tor software, or on the real (but modified) Tor implementation.
The advantage of the former is that it should be easier: only the timing of cells needs to be simulated and nodes have full visibility of the state of other nodes.
The advantage of the latter is that the simulation is more realistic.

In the absence of a suitable model of Tor, it has been decided to modify the existing Tor implementation.
The Tor network can then be emulated using ExperimenTor~\cite{experimentor} and Shadow~\cite{shadow}.
This does require implementing a datagram variant of Tor sufficiently well to allow clients to route traffic with realistic performance characteristics.

\subsection{End-to-end vs Hop-by-hop}

A design choice for a datagram variant of Tor is the end points of the reliability protocol.
Options include OP to destination server (\ie Tor routes IP packets), OP to exit node (\ie the exit node reassembles TCP streams and re-segments them), or hop-by-hop (\ie each node re-assembles streams).
OP to destination server is the most versatile, but makes implementing exit-policies difficult.
OP to exit node is the closest approximation of the end-to-end principle for Internet design, but means that any dropped packet will need to be retransmitted all the way along the circuit.
Hop-by-hop is not as versatile as either of the end-to-end variants, but by providing a reliable tunnel to all nodes it means that Tor's existing cell cryptography and relay protocol could remain unchanged.

The ease of implementation, coupled with the fact that evaluation has shown good results from the hop-by-hop approach make it the most suitable for further development.
End-to-end variants may however still be worthwhile to examing if time is available.

\subsection{Transport Protocol}

Reardon's original implementation used the TCP Daytona stack, which is not available under a Tor compatible license and so cannot be used.
However there are a number of more suitable alternatives.

\subsubsection{$\upmu$tp}

The libutp implementation is widely deployed in BitTorrent, although its performance goal of yielding to TCP is not ideal.

\subsubsection{FreeBSD network stack}

The FreeBSD network stack is being ported to userspace by Kip Macy, and will provide TCP and SCTP.

\subsubsection{SCTP}

A userspace SCTP stack is available\footnote{\url{http://sctp.fh-muenster.de/sctp-user-land-stack.html}}.

\subsubsection{CurveCP}

The CurveCP stack is available, although has not had as extensive testing as the other options.

\section{Development plans}

Future plans will need to encompass four areas.

\subsection{Network model}

A model of the simulated Tor network will need to be developed.
This should include node characteristics (CPU, possibly memory) and network characteristics (capacity, latency, other traffic usage).

\subsection{User/server model}

A model will need to be developed of the demands users put on the network, and how servers respond to requests.

\subsection{Tor implementation}

Tor will need to be updated to use each of the transport protocols to be tested.

\subsection{Metrics and Experimentation}

Finally, performance metrics will need to be developed, and experiments run.

\section{Schedule and participants}

Participants who have offered to help are:
\begin{itemize}
\item Rob Jansen (Shadow) working with Kevin Bauer (ExperimenTor) on modeling Tor.
\item Kevin and Mashael AlSabah will be getting Joel Reardon's code working on current Tor
\item Rob will be focusing on hop-by-hop transports, starting with $\upmu$TP. He is currently busy on another project, but will be able to return in early May.
\item Mashael will focus on end-to-end transports, including analyzing how it affects security
\item Steven Murdoch will try to pull everything together and do everything else
\end{itemize}


\label{sec:bib}
\bibliographystyle{plain}
%\bibliographystyle{alpha}
%\bibliographystyle{unsrt}
%\bibliographystyle{abbrv}
\bibliography{datagram-testing-plan}
\end{document}

% EOF
